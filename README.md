# Intersect

## Installation and Setup
- Clone GIT repository
- Navigate to the root directory of your project.
- Now you should be ready to bring up the Docker environment (must have Docker installed)  
```bash
docker-compose up -d
```
You should now be able to navigate to http://localhost:8080 in your browser once the environment is up.

## Useful Endpoints / Ports
- Site: http://localhost:8080
- PHPMyAdmin: http://localhost:8081
- MySQL Port: 3306

## Starting and Stopping Environment
Run the following Docker command to start load the environment inside a Docker container
```
docker-compose up -d
```

Run the following Docker command to tear down your environment and stop your Docker container
```
docker-compose down
```

If you want the some the Docker environment logs, run the following Docker command
```
docker-compose logs -f
```

## Updating Composer Dependencies
Run the following Docker command to install/update your Composer dependencies
```
docker-compose run --rm composer install
docker-compose run --rm composer update
```

## Running PHPUnit Tests
Make sure your Docker environment is started, if not, please see Starting and Stopping Environment section

#### Run
Run the following Docker command to run the PHPUnit tests
```
docker exec app vendor/bin/phpunit
```

#### Run with Coverage Report
If you want to generate a code-coverage report while running the PHPUnit tests, run the following Docker command
```
docker exec app vendor/bin/phpunit --coverage-html=./tests/coverage-results
```
This will create your coverage report inside the `tests/coverage-results` directory