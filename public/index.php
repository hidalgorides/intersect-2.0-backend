<?php

error_reporting(E_ALL);

require_once dirname(__FILE__) . '/../vendor/autoload.php';

use Intersect\Application;
use Intersect\Core\Http\Request;

$request = Request::initFromGlobals();
if ($request->getBaseUri() == '/favicon.ico')
{
    return;
}

$application = Application::instance();
$application->setBasePath(dirname(dirname(__FILE__)));
$application->handleRequest($request);
